export default function ({ store, req, redirect }) {
    if (process.server && !req) return;
    if (store.$auth.loggedIn) {
        return redirect('/admin');  
    }
}