import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  ssr:false,
  head: {
    //titleTemplate: '%s - ciudad-salud-front',
    title: 'Ciudad Salud',
    htmlAttrs: {
      lang: 'es'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  //activar para cuando se tenga login
  router: {
    middleware: ['auth']
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/vuetify.scss',
    'material-design-icons-iconfont/dist/material-design-icons.css',
  ],

  axios: {
    baseURL: process.env.API_URL,
    proxyHeaders: false,
    credentials: false,
  },
  
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/vee-validate.js', ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    [
      'nuxt-sweetalert2',
      {
        confirmButtonColor: '#2F80ED',
        cancelButtonColor: '#EB5757'
      }
    ],
    '@nuxtjs/auth-next'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      ligth: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: "#ec4667",
          accent: colors.grey.darken3,
          secondary: "#E8F6EF",
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
      }
      }
    }
  },

  auth: {
    // plugins: ['~/plugins/auth.js' ],
    strategies: {
      local: {
          // scheme: 'refresh',
          scheme: 'local',
          user: {
            property: ''
          },
          endpoints: {
              login: {
                  url: `/auth/login`,
                  method: 'post',
                  propertyName: 'data.accessToken',
              },
              refreshToken: {
                  property: 'data.accessToken',
                  method: 'get',
                  url: '/auth/refresh',
                  tokenRequired: true,
              },
              user: {
                  url: `/auth/profile`,
                  method: 'get',
                  propertyName: 'data.user',
                  autoFetch: true
              },
              forgotPassword: {
                  url: `/auth/forgotPassword`,
                  method: 'post',
                  propertyName: 'data.token',
              },
              logout: {
                  url: `auth/logout`,
                  method: 'post',
                  propertyName: 'data'
              },
          },
          token: {
            property: 'data.accessToken',
            global: true,
            // required: true,
            // type: 'Bearer'
          },
          // tokenType: 'Bearer',
          // tokenName: 'Authorization',
      }
    }
  },
  redirect: {
      login: '/login',
      logout: '/',
      user: '/',
      home: false,
  },
  cookie: {
      prefix: 'auth.',
      options: {
          path: '/login',
          expires: 1
      }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      "vee-validate/dist/rules"
    ],
  },
  
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    refresh_interval: process.env.JWT_EXPIRES || 540000,
  }
}
