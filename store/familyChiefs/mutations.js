const mutations = {
    setFamilyChiefs(state, payload) {
        state.familyChiefs = payload
    },
    toggleLoading(state, payload) {
        state.loading = payload
    },
    toggleOverlay(state, payload) {
        state.overlay = payload
    },
    destroyOneFamilyChief(state, payload) {
        let index = state.familyChiefs.map(function(e) { return e.id; }).indexOf(payload);
        state.familyChiefs.splice(index,1)
    },
    setOneFamilyChief(state, payload) {
        state.familyChief = payload
    },
}
export default mutations;
