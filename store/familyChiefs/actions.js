const actions = {
    async listFamilyChiefs({ commit }){

        commit('toggleLoading',true);
        await this.$axios.get(`/family-chief`).then(res => {
            commit('toggleLoading',false);
            commit('setFamilyChiefs', res.data);
            return res;
        }).catch(error => {
            return error;
        })
    },
    saveFamilyChief({ commit },payload){
        
        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.post(`/family-chief`,payload).then((res) => {
                commit('toggleOverlay',false);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },

    destroyFamilyChief({ commit },payload){
        
        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/family-chief/${payload}`).then((res) => {
                commit('toggleOverlay',false);
                commit('destroyOneFamilyChief',payload);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },

    getOneFamilyChief({ commit },payload){
        
        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.get(`/family-chief/${payload}`).then((res) => {
                commit('toggleOverlay',false);
                commit('setOneFamilyChief',res.data);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },

    editFamilyChief({ commit },payload){

        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.put(`/family-chief/${payload.id}`,payload).then((res) => {
                commit('toggleOverlay',false);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },
}

export default actions;