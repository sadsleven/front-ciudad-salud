const mutations = {
    setHomes(state, payload) {
        state.homes = payload
    },
    toggleLoading(state, payload) {
        state.loading = payload
    },
    toggleOverlay(state, payload) {
        state.overlay = payload
    },
    destroyOneHome(state, payload) {
        let index = state.homes.map(function(e) { return e.id; }).indexOf(payload);
        state.homes.splice(index,1)
    },
    setOneHome(state, payload) {
        state.home = payload
    },
}
export default mutations;
