const actions = {
    async listHomes({ commit }){

        commit('toggleLoading',true);
        await this.$axios.get(`/home`).then(res => {
            commit('toggleLoading',false);
            commit('setHomes', res.data);
            return res;
        }).catch(error => {
            return error;
        })
    },
    saveHome({ commit },payload){
        
        commit('toggleLoading',true);
        return new Promise((resolve, reject) => {
            this.$axios.post(`/home`,payload).then((res) => {
                commit('toggleLoading',false);
                resolve(res)
            }).catch(error => {
                commit('toggleLoading',false);
                reject(error.response.data)
            })
        })
    },

    destroyHome({ commit },payload){
        
        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/home/${payload}`).then((res) => {
                commit('toggleOverlay',false);
                commit('destroyOneHome',payload);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },

    getOneHome({ commit },payload){
        
        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.get(`/home/${payload}`).then((res) => {
                commit('toggleOverlay',false);
                commit('setOneHome',res.data);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },

    editHome({ commit },payload){

        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.put(`/home/${payload.id}`,payload).then((res) => {
                commit('toggleOverlay',false);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },
}

export default actions;