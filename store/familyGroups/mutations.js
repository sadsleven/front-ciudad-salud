const mutations = {
    setFamilyGroups(state, payload) {
        state.familyGroups = payload
    },
    toggleLoading(state, payload) {
        state.loading = payload
    },
    toggleOverlay(state, payload) {
        state.overlay = payload
    },
    destroyOneFamilyGroup(state, payload) {
        let index = state.familyGroups.map(function(e) { return e.id; }).indexOf(payload);
        state.familyGroups.splice(index,1)
    },
    setOneFamilyGroup(state, payload) {
        state.familyGroup = payload
    },
}
export default mutations;
