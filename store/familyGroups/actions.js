const actions = {
    async listFamilyGroups({ commit }){

        commit('toggleLoading',true);
        await this.$axios.get(`/family-group`).then(res => {
            commit('toggleLoading',false);
            commit('setFamilyGroups', res.data);
            return res;
        }).catch(error => {
            return error;
        })
    },
    saveFamilyGroup({ commit },payload){
        
        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.post(`/family-group`,payload).then((res) => {
                commit('toggleOverlay',false);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },

    destroyFamilyGroup({ commit },payload){
        
        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.delete(`/family-group/${payload}`).then((res) => {
                commit('toggleOverlay',false);
                commit('destroyOneFamilyGroup',payload);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },

    getOneFamilyGroup({ commit },payload){
        
        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.get(`/family-group/${payload}`).then((res) => {
                commit('toggleOverlay',false);
                commit('setOneFamilyGroup',res.data);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },

    editFamilyGroup({ commit },payload){

        commit('toggleOverlay',true);
        return new Promise((resolve, reject) => {
            this.$axios.put(`/family-group/${payload.id}`,payload).then((res) => {
                commit('toggleOverlay',false);
                resolve(res)
            }).catch(error => {
                commit('toggleOverlay',false);
                reject(error.response.data)
            })
        })
    },
}

export default actions;