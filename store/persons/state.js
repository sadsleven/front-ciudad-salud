export const state = () => ({
    persons:[],
    loading:false,
    overlay:false,
    person:{}
})
export default state;
