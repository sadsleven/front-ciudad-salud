const mutations = {
    setPersons(state, payload) {
        state.persons = []
        let isDone = false
        payload.forEach(person => {
            if (isDone){
                state.persons.push(person)
            }
            else {
                if (person.name == 'admin' && person.lastName == 'admin'  && person.dni == '123456789' && person.gender == 'Masculino' ){
                    isDone = true
                }
                else {
                    state.persons.push(person)
                }
            }            
        });
    },
    toggleLoading(state, payload) {
        state.loading = payload
    },
    toggleOverlay(state, payload) {
        state.overlay = payload
    },
    destroyOnePerson(state, payload) {
        let index = state.persons.map(function(e) { return e.id; }).indexOf(payload);
        state.persons.splice(index,1)
    },
    setOnePerson(state, payload) {
        state.person = payload
    },
}
export default mutations;
